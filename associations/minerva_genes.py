import common
import argparse
from typing import List, Dict, Set

def get_minerva_format(gene_symbols: List[str]) -> str:

    out = ""
    out += "#NAME=DISEASE_ASSOCIATED_GENES\n"
    out += "Name\tColor\n"

    for gs in set(gene_symbols):
        out += "{}\t#009E73\n".format(gs)

    return out

if __name__ == '__main__':

    common.init_logging()


    parser = argparse.ArgumentParser()

    parser.add_argument("-f", "--file",
                        required=True,
                        help="A file with comma-separated list of gene symbols")

    args = parser.parse_args()

    with open(args.file) as f:
        gene_symbols = f.read()

        print(get_minerva_format(gene_symbols.strip().split("\n")))
