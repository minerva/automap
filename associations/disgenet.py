import common
import logging
import json
import argparse
from typing import List, Dict
import requests

def get_genes(orphanet_ids:str, token:str, score:float) -> Dict[str, Dict]:

    genes = {}

    logging.info("Connecting to disgenet gda endpoint")
    res = requests.get(
        "http://www.disgenet.org/api/gda/disease/ordo/"+orphanet_ids,
        headers={"Authorization": "Bearer "+token},
        params={"min_score": score}
    )
    logging.info("Retrieved data from disgenet gda endpoint")
    if res.status_code == requests.codes.ok:
        content = json.loads(res.text)
        for rec in content:
            genes[rec['gene_symbol']] = {"association_score": rec['score'], "variants": []}

    return genes

def get_variants(orphanet_ids:str, token:str, genes: Dict[str, Dict]):

    logging.info("Connecting to disgenet vda endpoint")
    res = requests.get(
        "http://www.disgenet.org/api/vda/disease/ordo/"+orphanet_ids,
        headers={"Authorization": "Bearer "+token},
        params={"gene": ",".join(genes.keys())}
    )
    logging.info("Retrieved data from disgenet vda endpoint")

    if res.status_code == requests.codes.ok:
        content = json.loads(res.text)
        for rec in content:
            gene = rec['gene_symbol']
            for g in gene.split(';'):
                if g not in genes:
                    continue
                genes[g]['variants'].append(rec['variantid'])

if __name__ == '__main__':

    common.init_logging()

    parser = argparse.ArgumentParser()

    parser.add_argument("-oids", "--orphanet_ids",
                        required=True,
                        help="Input Orphanet numbers")
    parser.add_argument("-t", "--token",
                       required=True,
                       type=str,
                       help="Token required for the API call")
    parser.add_argument("-n", "--top-n",
                        required=False,
                        default=50,
                        type=int,
                        help="Retrieve the top N genes sorted by association score (0 for all)")
    parser.add_argument("-s", "--threshold-score",
                       required=False,
                       default=0,
                       type=float,
                       help="Only consider genes with association score higher than given threshold")

    args = parser.parse_args()

    genes = get_genes(args.orphanet_ids, args.token, args.threshold_score)
    get_variants(args.orphanet_ids, args.token, genes)
    genes_sorted = {}
    cnt = args.top_n
    i = 0
    for k, v in sorted(genes.items(), key=lambda x: x[1]['association_score'], reverse=True):
        if i >= cnt:
            break
        i += 1
        genes_sorted[k] = v

    print(json.dumps({"name": "disgenet", "genes": genes_sorted}, indent=2))






