library(dplyr)

### Read the file, and it does not exist - download it
if(!file.exists("variant_summary.txt.gz")) {
  download.file(url = "https://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variant_summary.txt.gz", 
                destfile = "variant_summary.txt.gz")
}

### Read in the table directly from the .gz file
cvtab <- read.table(gzfile("variant_summary.txt.gz"), header = T, sep = "\t", quote = "", comment.char = "")

### Reduce the table: 
### 1) keep entries with HGNC symbol, Assembly == GRCh37, dbSNP entry and non-benign clinical significance
### 2) keep entries having Orphanet ids in their PhenotypeIDS
### 3) select only essential columns
cvtab_red <- dplyr::filter(cvtab, HGNC_ID != "-" & Assembly == "GRCh37" & RS...dbSNP. != -1 &
                             !(grepl("[B|b]enign", ClinicalSignificance))) %>%
  dplyr::filter(grepl("Orphanet", PhenotypeIDS)) %>%
  dplyr::select(X.AlleleID, Type, GeneID, GeneSymbol, HGNC_ID, ClinicalSignificance, 
                RS...dbSNP., PhenotypeIDS,
                Assembly, Chromosome, Start, Stop)
### Split phenotype ids as they are a mix of different ontologies
phidsplit <- strsplit(cvtab_red$PhenotypeIDS, split = "[,|;]")
### Keep only HPO and Orphanet entries, add them as separate columns
ophpo <- purrr::map_df(phidsplit, 
                       ~ c(OrphanetID = paste(.[startsWith(., "Orphanet:")], collapse = ","),
                           HPOID = paste(.[startsWith(., "Human Phenotype Ontology:")], collapse = ",")))
final_tab <- cbind(cvtab_red, ophpo) %>% dplyr::select(-PhenotypeIDS)
### Write to file
write.table(final_tab, file = "OrphaHPO_clinvar_variants_summary.tsv", sep = "\t",
            col.names = T, row.names = F, quote = F)