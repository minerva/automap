# **get_extended_network.R** 
Script for extending the original list of disease genes using PPI data from STRING and OmniPath databases. 

Should be run as `Rscript --vanilla get_extended_network.R <args>`.  
Run with --help to see available options:

**--input_genes=CHARACTER** path to a list with input genes

**--nb_additional_nodes=NUMERIC** number of additional nodes fetched for the provided list

**--stringdb_score=NUMERIC** minimal score of StringDB interactions

**--output_file=CHARACTER** path to store results

The script provides an output file with the pairs gene A - gene B (HGNC identifiers, and also the ncbi entrez identifiers). If available in omnipath, it will contain directionality (values = 1, there is direction. value=0 means no direction ). The column consensus_directionality reflects the fact that some evidence for a pair might be contradictory. It can be ignored by now. The column references contains the publications retrieved from omnipath. 


