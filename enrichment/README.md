# **enrich_maps.R** 
Script for calculating enrichment for selected disease maps and pathway databases. 

Should be run as `Rscript --vanilla enrich_maps.R <args>`.  
Run with --help to see available options:

**--input_genes=CHARACTER** path to a list of inout genes to enrich, one per line

**--minervanet_ids=CHARACTER** a list of MINERVA Net identifiers, comma separated (see [minerva-net.lcsb.uni.lu](https://minerva-net.lcsb.uni.lu))

**--enricher_pathwaydb_names=CHARACTER** a list of EnrichR names for pathway databases to use in enrichment

**--output_file_maps=CHARACTER** path to store enriched disease map areas

**--output_file_pathways=CHARACTER** path to store enriched pathways

The script provides two output files with enrichment of a) disease maps and b) pathway databases specified as arguments. The tables have different formats, a) is produced by the `minervar` package, b) by `enrichr` package. Both contain source ID information and adjusted p values, allowing downstream filtering by significance and content download.