FROM gitlab.lcsb.uni.lu:4567/r3/docker/r-python

ADD ./dependencies /automap/dependencies

WORKDIR /automap

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl libssl-dev libxml2-dev gfortran libblas-dev liblapack-dev

RUN pip3 install -r dependencies/python_requirements.txt

RUN Rscript --vanilla dependencies/dependencies.R

CMD bash
