#!/usr/bin/env bash

# ------------------------- PARAMETERS TO SET -------------------------
# Load parameters either from parameters.sh, or from a file provided as the first parameter of the script
if [[ $1 == "" ]]; then
    source parameters.sh
else
    source $1
fi
# ------------------------- PARAMETERS TO SET -------------------------

check_exit_code(){
    if [ $? -ne 0 ]; then
        tput setaf 1; echo "The last command failed. Can't continue..."
        exit
    fi
}

log() {
    echo `date +"%T"`: $1
}

ASSOCIATIONS_DIR=associations/
ASSOCIATIONS_DATA_DIR=$ASSOCIATIONS_DIR/data/
EXTEND_DIR=${ASSOCIATIONS_DIR}/extend_network/
RES_DIR=results
ENRICHMENT_DIR=enrichment
MERGING_DIR=merging/
UTIL_DIR=utils/
PYTHON_BIN=python3
PIP_BIN=pip3

DISGENET_ASSOCIATION_SCORE_THRESHOLD_STR=${DISGENET_ASSOCIATION_SCORE_THRESHOLD//./_}
OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD_STR=${OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD//./_}

if [ ${USE_HPO} = 1 ]; then
    log "Inferring OrphaNet identifiers based on HPO identifiers..."
    ORPHANET_IDS=$(Rscript --vanilla ${ASSOCIATIONS_DIR}/hpo_match//hpo_to_ordo.R --hpos ${HPO_IDS} --max_ordos ${MAX_INFERRED_ORPHANET_IDS})
    log "Inferred OrphaNet identifiers: ${ORPHANET_IDS}"
fi

ORPHANET_IDS_UNDERSCORE=${ORPHANET_IDS//,/_}

mkdir $RES_DIR
RES_DIR=$RES_DIR/${ORPHANET_IDS_UNDERSCORE}
mkdir $RES_DIR

# First we copy the parameters into the result directory
cp parameters.sh ${RES_DIR}

out_paths=""
if [ ${USE_DISGENET} = 1 ]; then
    log "Querying DisGeNET..."
    disgenet_out_path=${RES_DIR}/01-disgenet-n_${DISGENET_CNT_THRESHOLD}-s_${DISGENET_ASSOCIATION_SCORE_THRESHOLD_STR}.json
    $PYTHON_BIN $ASSOCIATIONS_DIR/disgenet.py -oids ${ORPHANET_IDS} -t ${DISGENET_TOKEN} -n ${DISGENET_CNT_THRESHOLD} -s ${DISGENET_ASSOCIATION_SCORE_THRESHOLD} > ${disgenet_out_path}
    check_exit_code
    log "DisGeNET gene and variant associations stored in ${disgenet_out_path}"
    out_paths="${out_paths},${disgenet_out_path}"
fi


if [ ${USE_OPENTARGETS} = 1 ]; then
    # Get associations from OpenTargets
    log "Querying OpenTargets..."
    opentargets_out_path=${RES_DIR}/01-opentargets-n_${DISGENET_CNT_THRESHOLD}-s_${OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD_STR}.json
    $PYTHON_BIN $ASSOCIATIONS_DIR/opentargets.py -o ${ORPHANET_IDS} -n ${OPENTARGETS_CNT_THRESHOLD} -s ${OPENTARGETS_ASSOCIATION_SCORE_THRESHOLD} > ${opentargets_out_path}
    check_exit_code
    log "Opentargets gene and variant associations stored in ${opentargets_out_path}"
    out_paths="${out_paths},${opentargets_out_path}"
fi

# Merge with ClinVar
log "Merging with ClinVar..."
genes_variants_out_path=${RES_DIR}/02-genes_variants.log
out_paths=${out_paths#,}
$PYTHON_BIN $ASSOCIATIONS_DIR/merge_with_clinvar.py -v $out_paths -c ${ASSOCIATIONS_DATA_DIR}/OrphaHPO_clinvar_variants_summary.tsv.zip -oid ${ORPHANET_IDS} > ${genes_variants_out_path}
check_exit_code
log "Integration with ClinVar stored in ${genes_variants_out_path}"

genes_line=`cat ${genes_variants_out_path} | grep "genes in total"`
genes_out_path=${genes_variants_out_path/02-genes_variants/03-genes}
echo ${genes_line#*:} | tr ',' '\n' > ${genes_out_path}
if [ ${USE_DEGS} = 1 ]; then
    log "Extending the gene list by provided DEGs (genes symbols from the first column)..."
    sed 1d $DEG_FILE | cut -f 1 >> ${genes_out_path}
    log "Gene list extended by DEGs in ${DEG_file}"
fi
sort -u ${genes_out_path} -o ${genes_out_path}
log "Genes stored in ${genes_out_path}"

log "Extending genes list..."
text_mining_out_path=${genes_out_path/03-genes/03-text-mining}
Rscript --vanilla ${EXTEND_DIR}/get_extended_network.R --input_genes ${genes_out_path} --nb_additional_nodes ${STRING_NETWORK_SIZE} \
--stringdb_score ${STRING_NETWORK_SCORE} --output_file ${text_mining_out_path}
check_exit_code
echo ${text_mining_out_path}
log "Genes list extended"

minerva_genes_out_path=${RES_DIR}/04-minerva-genes.txt
$PYTHON_BIN $ASSOCIATIONS_DIR/minerva_genes.py -f ${genes_out_path} > ${minerva_genes_out_path}
check_exit_code
log "Genes stored in ${minerva_genes_out_path}"

var_line=`cat ${genes_variants_out_path} | grep "variants in total"`
variants_out_path=${genes_variants_out_path/02-genes_variants/03-variants}
#echo ${var_line#*:} | sed 's/\,/\n/g' > ${variants_out_path}
echo ${var_line#*:} | tr ',' '\n' > ${variants_out_path}
log "Variants stored in ${variants_out_path}"

if [ ${USE_VEP} = 1 ]; then
    # Get associations from OpenTargets
    log "Filtering by allele frequency via Ensemble VEP service..."
    variants_vep_out_path=${variants_out_path/-variants/-varaints_vep}
    $PYTHON_BIN $ASSOCIATIONS_DIR/vepmining/vepAllInOne.py -f ${variants_out_path} -t ${VEP_THRESHOLD}> ${variants_vep_out_path}
    check_exit_code
    log "Fitlered variants stored in ${variants_vep_out_path}"
    variants_out_path=${variants_vep_out_path}
fi

log "Getting detailed variants information..."
minerva_variants_out_path=${RES_DIR}/04-minerva-variants.txt
$PYTHON_BIN $ASSOCIATIONS_DIR/minerva_variants.py -f ${variants_out_path} > ${minerva_variants_out_path}
log "Detailed variants information obtained"
check_exit_code

if [ ${STOP_AFTER_STAGE} = 1 ]; then
    echo "Exiting after stage ${STOP_AFTER_STAGE}"
    exit 0
fi
# ------------------------------ 2. Obtain pathways ------------------------------
log "Retrieving enriched pathways..."

enriched_maps_out_path=$RES_DIR/05-enriched_disease_maps.txt
enriched_paths_out_path=$RES_DIR/05-enriched_pathways.txt

Rscript --vanilla ${ENRICHMENT_DIR}/enrich_maps.R --input_genes ${genes_out_path} \
--minervanet_ids ${DISEASE_MAPS} --output_file_maps ${enriched_maps_out_path} \
--enricher_pathwaydb_names ${PATHWAY_DBS} --output_file_pathways ${enriched_paths_out_path}
check_exit_code

log "Enriched pathways retrieved"

if [ ${STOP_AFTER_STAGE} = 2 ]; then
    echo "Exiting after stage ${STOP_AFTER_STAGE}"
    exit 0
fi

# ------------------------------ 3. Integrate pathways ------------------------------
log "Assembling the map from pathways ..."
map_out_path=${RES_DIR}/06-minerva_map.xml
Rscript --vanilla ${MERGING_DIR}/merge_diagrams.R --maps ${enriched_maps_out_path} --max_dmap_areas ${ENRICH_MAX_AREAS_PER_MAP} \
--pathways ${enriched_paths_out_path} --max_pathways ${MAX_AREAS_PER_PATHWAY_DB} --textmining ${text_mining_out_path} \
--output_file ${map_out_path} --debug_files TRUE

check_exit_code

log "Pathways assembled into ${map_out_path}"

log "Implanting UniProt annotations..."
map_out_path_uniprot=${map_out_path/.xml/_unp.xml}
$PYTHON_BIN ${UTIL_DIR}/implant_annotations.py -m ${map_out_path} -v ${minerva_variants_out_path} > ${map_out_path_uniprot}
check_exit_code
log "UniProt annotations implanted"

log "Combining the map with overlays"

tmp_dir=${RES_DIR}/tmp/
tmp_dir_overlays=${tmp_dir}/overlays/

mkdir ${tmp_dir}
cp ${map_out_path_uniprot} ${tmp_dir}

mkdir ${tmp_dir_overlays}
cp ${minerva_genes_out_path} ${tmp_dir_overlays}
cp ${minerva_variants_out_path} ${tmp_dir_overlays}

map_zip_out_path=${map_out_path_uniprot/.xml/.zip}
rm ${map_zip_out_path}
cd ${tmp_dir}
zip -r tmp.zip .
cd -
mv ${tmp_dir}/tmp.zip ${map_zip_out_path}

rm -rf ${tmp_dir}
log "Final map with overlays stored in ${map_zip_out_path}"