import xml.etree.ElementTree as ET
import re
from typing import Dict

def register_namespaces(path) -> Dict:
    namespaces = {}
    with open(path) as f:
        content = f.read()
        # sbml_tag = re.search(r'<sbml([^>]*)>', content).group(1)
        # for ns in re.finditer(r'xmlns:*([^=]*)="([^"]*)"', sbml_tag ):
        for ns in re.finditer(r'xmlns:*([^=]*)="([^"]*)"', content):
            k = ns.group(1)
            v = ns.group(2)
            if k in namespaces:
                continue
            # if not name:
            #     name = "core"
            ET.register_namespace(k, v)
            namespaces[k] = v
    return namespaces