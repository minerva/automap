# `Automap` workflow for ad-hoc construction of rare disease maps

This workflow was inspired by our earlier work during BioHackathon Europe 2019, as illustrated [here](https://r3.pages.uni.lu/biocuration/resources/biohackathon2019/rdmaps/), and described in our preprint ([doi:10.37044/osf.io/gmbjv](https://doi.org/10.37044/osf.io/gmbjv)).

The workflow is a sequence of steps for retrieval of genes related to a given rare disease, identification of pathways and disease map parts relevant for these genes and their assembly into a functional molecular map. This map is ready for visualization online using the MINERVA Platform.

## Pipeline description

The pipeline consists of a bunch of tools which can be divided into three broad categories:

1. [Retrieval of gene-disease mapping and variants](associations/README.md).
2. [Enrichment](enrichment/README.md)
3. [Map assembly](merging/README.md)

The tools are glued together by the `assembly.sh` script resulting in the following pipeline:

1. **Disease context:** Identify disease-related genes and variants for a given RD

    a. Get gene- and variant-disease mapping from DisGeNET and OpenTargets
    **Configurable parameters:**
    -- OrphaNet or HPO identifiers
    -- DisGeNET maximum number and score for retrieved genes

    **b.** OpenTargets association score threshold

    **c.** Get pathogenic variants and genes for the disease from ClinVar

    **d.** Filter out variants with high allele frequency using Ensemble's VEP service
    **Configurable parameters:**
    -- VEP threshold

    **e.** Compile a summary list of genes and variants associated with the disease
2. **Network of mechanisms:** Collect disease maps, pathways and networks enriched for disease-related genes

    **a.** Get enriched diagrams from disease maps
    **Configurable parameters:**
    -- Disease map instances (MINERVA Net identifiers)
    -- Maximum number of retrieved diagrams

    **b.** Get enriched diagrams from pathway databases
    **Configurable parameters:**
    -- Pathway databases (EnrichR identifiers)
    -- Maximum number of retrieved diagrams per database

    **c** Construct a text mining network using STRING and OmniPath
    **Configurable parameters:**
    -- Maximum number of new neighbors in STRING network
    -- Maximum score of retrieved STRING interactions

3. **Interactive prototype:** Assemble and visualize the prototype map

    **a.** Compile the obtained pathways into a single diagram

    **b.** Store gene names for data  overlay in the MINERVA Platform

    **c.** Store variant information (position, protein-level mapping) for genetic variant overlay in the MINERVA Platform

    **d** Bundle the disease map with genetic and variant overlays into a single archive to be then uploaded to [the MINERVA Platform](https://minerva.pages.uni.lu/).

## Example

Directory [example](example/) contains results for the runs for OrphaNet 2331 (Kawasaki disease) and 791 (retinitis pigmentosa) on 07/02/2023.

## Run in a container (recommended)

Before running the pipeline locally, please install [Docker Desktop](https://docs.docker.com/desktop/). Then, run:

```bash
docker run -it -v$(pwd):/automap gitlab.lcsb.uni.lu:4567/minerva/automap /bin/bash assemble.sh
```

You can also provide additional parameters, being the parameters file if it differs from `parameters.sh`.

This will run the pipeline described in the next section and eventually output a
ZIP file with a map which can then be imported in [the MINERVA Platform](https://minerva.pages.uni.lu/doc/)
as a disease maps integrating all the found enriched pathways together with
genetic and variant [overlays](https://minerva.pages.uni.lu/doc/user_manual/v16.0/index/#overlays-tab).

You can also check the result:

```bash
curl 'https://minerva-service.lcsb.uni.lu/minerva/api/convert/image/CellDesigner_SBML:pdf' -X POST \
    -T ./06-minerva_map_unp.xml \
    -H 'Content-Type: application/octet-stream' > out.pdf
```

## Run natively (development)

### Dependencies

- Bash
- Python 3.x
- R

#### Python

Python needs packages defined in `dependencies/python_requirements.txt` which can be installed
via

```commandline
pip3 install -r dependencies/python_requirements.txt
```

#### R

The R scripts which are part of the pipeline should be able to install their dependencies on the first run. However,
it might happen that elevated privileges will be needed. If that is the case and the pipeline fails
on the R scripts execution, you can install the dependencies by running:

```bash
sudo Rscript --vanilla dependencies/dependencies.R
```

#### System

If the pipeline is run on a clean Linux installation you might need to install the following libraries
(`sudo apt-get install` on Ubuntu) prior to running the code:

- curl
- libssl-dev
- libxml2-dev

### Pipeline execution

To execute the pipeline, set the values of parameters in the `parameters.sh`, mainly
the list of [Orphanet](https://www.orpha.net/consor/cgi-bin/index.php?lng=EN) disease numbers.
When the parameters are set, run the pipeline:

```
bash assemble.sh
```
