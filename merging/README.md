# Merging map diagrams

This R code assemble resources into a single map. And consists of:

1. Assembling the input pathways into a SBML-formatted map file.
2. Postprocessing of the map file.

## 1. Assembling the input pathways 

**merge_diagrams.R** function is used, which should be run as `Rscript --vanilla merge_diagrams.R <args>`.  
Run with --help to see available options:

**--maps=CHARACTER** path to table with enriched disease maps

**--map_adj_pval=NUMERIC** max adj p value of disease maps enriched areas

**--max_dmap_areas=NUMERIC** max number of disease maps enriched areas

**--pathways=CHARACTER** path to table with enriched pathways

**--pathway_adj_pval=NUMERIC** max adj p value of enriched pathways

**--max_pathways=NUMERIC** max number of enriched pathways

**--textmining=CHARACTER** path to table with text mining results

**--output_file=CHARACTER** path to store the output

**--debug_files=LOGICAL** store temporary merging zip files

### Description

Disease maps, text mining results and pathways are assembled separately. Then the resulting three parts are combined together. In case of limited number of genes some or all parts may be missing.

The final xml file (CellDewigner SBML) has long attribute values trimmed (up to 255 characters). 

## 2. Map file post-processing

The resulting file needs to be modified in order to attach UniProt identifiers to the species.

Attaching UniProt identifiers is necessary in order for the proteins to be UniProt annotated. The reason is that the genetic variant overlay (generated in the previous step of the pipeline) is mapped on the proteins based on both gene name and UniProt annotation. So if a variant maps on a protein only based on gene level but not on UniProt level, it won't show in MINERVA.

Usage:

```bash
python3 utils/implant_annotations.py -m map_file -v minerva_variants_file
```